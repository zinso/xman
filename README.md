# xman is bing search cli
Rust program to display top Bing search engine results

[![Crates.io](https://img.shields.io/crates/v/xman.svg)](https://crates.io/crates/xman)
[![Rust](https://img.shields.io/badge/rust-1.56.1%2B-blue.svg?maxAge=3600)](https://framagit.org/zinso/xman)
[![license](https://img.shields.io/badge/license-MIT-blue.svg)](https://framagit.org/zinso/xman/-/raw/master/LICENSE)
![xman.gif](https://framagit.org/zinso/xman/-/raw/master/xman.gif)
## Basic usage
```sh
# bing search("rust book filetype:pdf") and show 30 results
xman rust book filetype:pdf 30
# bing search("javascript crash course") default search result is 100
xman javascript crash course 
```
## install
```sh
cargo install xman
```

### Arguments

- **query** the thing you usually put in address bar, like Google
    but not _Evil_
- **max_result** reasonably short/long output is not required defalut value 100.